#!/usr/bin/env python3

import argparse
from pathlib import Path
from xml.etree import ElementTree
from copy import copy

import mutagen


def _add_tag_value(metadata, tag, value):
    if tag not in metadata:
        metadata[tag] = value
        return
    if value not in metadata[tag]:
        metadata[tag] += [value]


def handle_music_file(filename, metadata):
    print(filename)
    tree = ElementTree.parse(filename)
    music = tree.getroot()
    music_data = dict(music.attrib)

    print(music_data)

    music_path = filename.parent / music_data['file']
    new_path = Path.cwd() / music_path.name
    print(music_path)
    with open(music_path, 'rb') as f:
        with open(new_path, 'wb') as t:
            t.write(f.read())

    tags = mutagen.File(str(new_path))
    _add_tag_value(tags, 'title', music_data['title'])
    for artist in music_data['composer'].split('&'):
        artist = artist.strip()
        _add_tag_value(tags, 'artist', artist)
    _add_tag_value(tags, 'album', metadata['Upstream-Name'])
    _add_tag_value(tags, 'organizationcontact', metadata['Upstream-Contact'])
    _add_tag_value(tags, 'organization', metadata['Source'])
    _add_tag_value(tags, 'url', metadata['Source'])
    if music_path in metadata['per-file']:
        filemeta = metadata['per-file'][music_path]
        if 'Copyright' in filemeta:
            if filemeta['Copyright'] != 'NA':
                _add_tag_value(tags, 'copyright', filemeta['Copyright'])
        if 'License' in filemeta:
            if filemeta['License'] != 'NA':
                _add_tag_value(tags, 'license', filemeta['License'])
    tags.save()
    print(tags)


def dir_metadata(dirpath):
    licences = dirpath / 'licenses.txt'
    with open(licences, 'r') as f:
        data = f.read()

    def parse_paragraph(paragraph):
        para = {}
        for line in paragraph.splitlines():
            fieldname, value = line.split(':', maxsplit=1)
            fieldname = fieldname.strip()
            value = value.strip()
            para[fieldname] = value
        return para

    paragraphs = data.split('\n\n')
    header = parse_paragraph(paragraphs[0])
    files = [parse_paragraph(p) for p in paragraphs[1:]]
    metadata = copy(header)
    metadata['per-file'] = {}
    for filedata in files:
        file_metadata = copy(filedata)
        del file_metadata['Files']
        for filepath in dirpath.glob(filedata['Files']):
            if str(filepath) not in metadata['per-file']:
                metadata['per-file'][filepath] = {}
            metadata['per-file'][filepath].update(file_metadata)

    print(metadata)
    return metadata


def dir_list(dirpath):  # type: (Path) -> Any
    return dirpath.glob('*.music')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('music_dir', type=Path)
    args = parser.parse_args()
    dirpath = args.music_dir
    metadata = dir_metadata(dirpath)
    file_list = dir_list(dirpath)
    for filename in file_list:
        handle_music_file(filename, metadata)


if __name__ == '__main__':
    main()
